# Bitbucket Pipelines Pipe: AWS Lambda Deploy
Update Lambda function code and create new version, and create/update aliases pointing to versions.


The pipe is based on [AWS Boto3 lambda][boto3 lambda].


## YAML Definition
Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: atlassian/aws-lambda-deploy:1.12.0
  variables:
    AWS_ACCESS_KEY_ID: '<string>' # Optional if already defined in the context or OIDC used.
    AWS_SECRET_ACCESS_KEY: '<string>' # Optional if already defined in the context or OIDC used.
    AWS_DEFAULT_REGION: '<string>' # Optional if already defined in the context or OIDC used.
    AWS_OIDC_ROLE_ARN: "<string>" # Optional by default. Required for OpenID Connect (OIDC) authentication.
    FUNCTION_NAME: '<string>'
    COMMAND: '<string>' # 'alias' or 'update'
    # AWS_ROLE_ARN: '<string>' # Optional.
    # AWS_ROLE_SESSION_NAME: '<string>' # Optional.
    
    # Update variables
    # ZIP_FILE: '<string>'
    # IMAGE_URI: '<string>'
    # S3_BUCKET: '<string>'
    # S3_KEY: '<string>' # Required if S3_BUCKET variable is provided.
    # S3_OBJECT_VERSION: '<string>' # Optional by default.
    # PUBLISH_FLAG: '<boolean>'

    # Alias variables
    # ALIAS: '<string>'
    # VERSION: '<string>'
    # DESCRIPTION '<string>' # Optional, alias command specific.

    # Common variables
    # FUNCTION_CONFIGURATION: "<string>" # Optional.
    # WAIT: '<boolean>' # Optional.
    # WAIT_INTERVAL: '<integer>' # Optional.
    # DEBUG: '<boolean>' # Optional.
```

## Variables
The following variables are required:

| Variable                   | Usage                                                                                                                                                                                                                                                                                                     |
|----------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| AWS_ACCESS_KEY_ID (**)     | AWS access key.                                                                                                                                                                                                                                                                                           |
| AWS_SECRET_ACCESS_KEY (**) | AWS secret key.                                                                                                                                                                                                                                                                                           |
| AWS_DEFAULT_REGION (**)    | The AWS region code (us-east-1, us-west-2, etc.) of the region containing the AWS resource(s). For more information, see [Regions and Endpoints][Regions and Endpoints] in the _Amazon Web Services General Reference_.                                                                                   |
| AWS_OIDC_ROLE_ARN          | The ARN of the role used for web identity federation or OIDC. See **Authentication**.                                                                                                                                                                                                                     |
| FUNCTION_NAME (*)          | Name or ARN of the function.                                                                                                                                                                                                                                                                              |
| COMMAND (*)                | The operation to perform. Valid options are 'update' or 'alias'.                                                                                                                                                                                                                                          |
| AWS_ROLE_ARN               | Specifies the Amazon Resource Name (ARN) of an IAM role with a web identity provider that you want to use to run the AWS commands.                                                                                                                                                                        |
| AWS_ROLE_SESSION_NAME      | Specifies the name to attach to the role session. This value is provided to the RoleSessionName parameter when the AWS CLI calls the AssumeRole operation, and becomes part of the assumed role user ARN: arn:aws:sts::123456789012:assumed-role/role_name/role_session_name.                             |
| ZIP_FILE (1)               | Path to the zip file containing the function code. Required for 'update' if IMAGE_URI or S3_BUCKET variables are not passed. Used by default, if alternatives IMAGE_URI or S3_BUCKET are not provided.                                                                                                    |
| IMAGE_URI (1)              | Uri of a container image in AWS ECR registry to be deployed from. Required for 'update' if ZIP_FILE or S3_BUCKET variables are not passed. Checkout more details about [Lambda image configuration][Lambda image configuration].                                                                          |
| S3_BUCKET (1)              | AWS S3 bucket name. Required for 'update' if ZIP_FILE or S3_BUCKET variables are not passed.                                                                                                                                                                                                              |
| S3_KEY                     | AWS S3 key or path-like directory structure of the deployment package. Required if S3_BUCKET variable is provided.                                                                                                                                                                                        |
| S3_OBJECT_VERSION          | The version of the deployment package object on AWS S3 to use, for versioned objects only.                                                                                                                                                                                                                |
| ALIAS                      | Alias to create or update. Required for 'alias'.                                                                                                                                                                                                                                                          |
| VERSION                    | The version of the function that the alias will point to. Required for 'alias'. If not provided, the pipe will try to fetch this version automatically (only available after you run the pipe in `update` mode in one of the previous steps).                                                             |
| PUBLISH_FLAG               | Flag to make publishing new function version configurable. Default: `True`.                                                                                                                                                                                                                               |
| FUNCTION_CONFIGURATION     | Path to a json file containing an function configuration parameters to update. Default: `null`. If provided, the pipe will update the function configuration. The format of the json file is the same as _Request Syntax_ for [boto3 update_function_configuration][boto3 update_function_configuration]. |
| WAIT                       | Wait for lambda update to complete. Default: `false`. If `true`, it will finish when the lambda deploy is successfully updated.                                                                                                                                                                           |
| WAIT_INTERVAL              | Time to wait between polling lambda to be with LastUpdateStatus `Successful` (in seconds). Default: `5`.                                                                                                                                                                                                  |
| DESCRIPTION                | Additional args to pass to `create_alias`, `update_alias`.                                                                                                                                                                                                                                                |
| DEBUG                      | Turn on extra debug information. Default: `false`.                                                                                                                                                                                                                                                        |
_(*) = required variable. This variable needs to be specified always when using the pipe._
_(**) = required variable. If this variable is configured as a repository, account or environment variable, it doesn’t need to be declared in the pipe as it will be taken from the context. It can still be overridden when using the pipe._
_(1) = required variable. Required one of the multiple options._


## Authentication

Supported options:

1. Environment variables: AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY. Default option.

2. Assume role provider with OpenID Connect (OIDC). More details in the Bitbucket Pipelines Using OpenID Connect guide [Integrating aws bitbucket pipeline with oidc][aws-oidc]. Make sure that you set up OIDC before:
    * configure Bitbucket Pipelines as a Web Identity Provider in AWS
    * attach to provider your AWS role with required policies in AWS
    * set up a build step with `oidc: true` in your Bitbucket Pipelines
    * pass AWS_OIDC_ROLE_ARN (*) variable that represents role having appropriate permissions to execute actions on Lambda resources


## Details
This pipe checks for lambda to be in `Active` or `Inactive` State and LastUpdateStatus not equal `InProgress` before start executing.

Information about lambda states: [AWS lambda states][Lambda states].


### Update Lambda Code
The pipe will update the named Lambda function with the code contained within a provided zip file, and atomically publish a new version. Both the new version 
and the built-in $LATEST version will point to the updated Lambda.

The pipe will create an `aws-lambda-deploy-env` file under the `$BITBUCKET_PIPE_SHARED_STORAGE_DIR` directory when executing the 'update' command, containing the `FunctionArn`, `Version` and other [function data in a json format][Update response details of the lambda function].

The version can be read from this file, and used in later pipes or commands.


### Create or Update Lambda Alias
Create or update an alias to point to a specific version of a Lambda function.


## Prerequisites
For Lambda updates, you will need:

  * An existing Lambda function
  * An IAM user configured with programmatic access or OIDC role that can perform the following operations on your function:
    * lambda:PublishVersion
    * lambda:UpdateFunctionCode
    * lambda:GetFunction

For Lambda alias, you will need:

  * An existing Lambda function
  * An IAM user has been configured with programmatic access that can perform the following operations on your function:
    * lambda:GetAlias
    * lambda:UpdateAlias
    * lambda:CreateAlias  


## Examples

### Update a Lambda

Update the function 'my-lambda-function' in region 'us-east-1' with the function code contained in 'my-function-code.zip'.
```yaml
script:
  - pipe: atlassian/aws-lambda-deploy:1.12.0
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: $AWS_DEFAULT_REGION
      FUNCTION_NAME: 'my-lambda-function'
      COMMAND: 'update'
      ZIP_FILE: 'my-function-code.zip'
```

Perform update with the assumed role user ARN.
```yaml
script:
  - pipe: atlassian/aws-lambda-deploy:1.12.0
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: $AWS_DEFAULT_REGION
      AWS_ROLE_ARN: 'arn:aws:iam::012349999999:role/my-assumed-role'
      AWS_ROLE_SESSION_NAME: 'my-assumed-role'
      FUNCTION_NAME: "my-lambda-function"
      COMMAND: 'update'
      ZIP_FILE: 'my-function-code.zip'
```

Update the function 'my-lambda-function' in region 'us-east-1' with the function code contained in 'my-function-code.zip' and wait until lambda update is successful.
```yaml
script:
  - pipe: atlassian/aws-lambda-deploy:1.12.0
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: $AWS_DEFAULT_REGION
      FUNCTION_NAME: 'my-lambda-function'
      COMMAND: 'update'
      ZIP_FILE: 'my-function-code.zip'
      WAIT: 'true'
```

Update the function 'my-lambda-function' in region 'us-east-1' with the function code contained in 'my-function-code.zip' stored in AWS S3 Bucket.
```yaml
script:
  - pipe: atlassian/aws-lambda-deploy:1.12.0
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: $AWS_DEFAULT_REGION
      FUNCTION_NAME: 'my-lambda-function'
      COMMAND: 'update'
      S3_BUCKET: 'my-bucket-name'
      S3_KEY: 'my-function-code.zip'
```

Update the function 'my-lambda-function' in region 'us-east-1' with the function code contained in 'my-function-code.zip'. `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY` and `AWS_DEFAULT_REGION` are configured as repository variables, so there is no need to declare them in the pipe.
```yaml
script:
  - pipe: atlassian/aws-lambda-deploy:1.12.0
    variables:
      FUNCTION_NAME: 'my-lambda-function'
      COMMAND: 'update'
      ZIP_FILE: 'my-function-code.zip'
```

Update the function 'my-lambda-function' in region 'us-east-1' with the function code from ECR image identified by image uri. `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY` and `AWS_DEFAULT_REGION` are configured as repository variables, so there is no need to declare them in the pipe. 
```yaml
script:
  - pipe: atlassian/aws-lambda-deploy:1.12.0
    variables:
      FUNCTION_NAME: 'my-lambda-function'
      COMMAND: 'update'
      IMAGE_URI: '${AWS_ACCOUNT}.dkr.ecr.${AWS_DEFAULT_REGION}.amazonaws.com/my-lambda-ecr-repository:image-tag'
```

Update the function 'my-lambda-function' with OpenID Connect (OIDC) alternative authentication without required `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`.
Parameter `oidc: true` in the step configuration and variable `AWS_OIDC_ROLE_ARN` are required:
```yaml

- step:
    oidc: true
    script:
      - pipe: atlassian/aws-lambda-deploy:1.12.0
        variables:
          AWS_DEFAULT_REGION: $AWS_DEFAULT_REGION
          AWS_OIDC_ROLE_ARN: 'arn:aws:iam::123456789012:role/role_name'
          FUNCTION_NAME: 'my-lambda-function'
          COMMAND: 'update'
          ZIP_FILE: 'my-function-code.zip'
```

Update the function 'my-lambda-function' without publishing a new version:
```yaml

script:
  - pipe: atlassian/aws-lambda-deploy:1.12.0
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: $AWS_DEFAULT_REGION
      FUNCTION_NAME: 'my-lambda-function'
      COMMAND: 'update'
      ZIP_FILE: 'my-function-code.zip'
      PUBLISH_FLAG: 'false'
```

Update the function 'my-lambda-function' with a new configuration provided in 'new_function_configuration.json' with custom provided wait interval and with the function code contained in 'my-function-code.zip'.
```yaml
script:
  - pipe: atlassian/aws-lambda-deploy:1.12.0
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: $AWS_DEFAULT_REGION
      FUNCTION_NAME: 'my-lambda-function'
      COMMAND: 'update'
      ZIP_FILE: 'my-function-code.zip'
      FUNCTION_CONFIGURATION: 'new_function_configuration.json'
      WAIT_INTERVAL: '15'
```

### Create or Update an Alias
Update an alias named 'production' to point to version 2 of function 'my-lambda-function':
```yaml
script:
  - pipe: atlassian/aws-lambda-deploy:1.12.0
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: $AWS_DEFAULT_REGION
      FUNCTION_NAME: 'my-lambda-function'
      COMMAND: 'alias'
      ALIAS: 'production'
      VERSION: '2'
```

Update an alias named 'production' to point to version 2 of function 'my-lambda-function' and add a description:
```yaml
script:
  - pipe: atlassian/aws-lambda-deploy:1.12.0
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: $AWS_DEFAULT_REGION
      FUNCTION_NAME: 'my-lambda-function'
      COMMAND: 'alias'
      ALIAS: 'production'
      VERSION: '2'
      DESCRIPTION: 'New functionality'
```

### Combining Update with Alias
A common use case is to update a Lambda function and publish a version, then point a new alias to the newly published function.
This allows you to create sophisticated Continuous Delivery workflows for your functions.
```yaml
script:
  
  # Build Lambda
  - build-lambda.sh
  
  # Update lambda code and publish a new version
  - pipe: atlassian/aws-lambda-deploy:1.12.0
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: $AWS_DEFAULT_REGION
      FUNCTION_NAME: 'my-lambda-function'
      COMMAND: 'update'
      ZIP_FILE: 'my-lambda.zip'
      WAIT: 'true'

  # Read results from the update pipe into environment variables
  - BITBUCKET_PIPE_SHARED_STORAGE_DIR="/opt/atlassian/pipelines/agent/build/.bitbucket/pipelines/generated/pipeline/pipes"
  - VERSION=$(jq  --raw-output '.Version' $BITBUCKET_PIPE_SHARED_STORAGE_DIR/aws-lambda-deploy-env)

  # Point an alias to the new lambda version
  - pipe: atlassian/aws-lambda-deploy:1.12.0
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: $AWS_DEFAULT_REGION
      FUNCTION_NAME: 'my-lambda-function'
      COMMAND: 'alias'
      ALIAS: 'production'
      VERSION: '${VERSION}'       
```

### Use Bitbucket Pipelines Deployments
In this scenario, we update and publish a Lambda, then use aliases to move the Lambda through test, staging and production deployment environments.
```yaml
- step:
    name: Build Lambda
    script:
    # Build Lambda
    - build-lambda.sh
    # Update lambda code and publish a new version
    - pipe: atlassian/aws-lambda-deploy:1.12.0
      variables:
        AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
        AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
        AWS_DEFAULT_REGION: $AWS_DEFAULT_REGION
        FUNCTION_NAME: 'my-lambda-function'
        COMMAND: 'update'
        ZIP_FILE: 'my-lambda.zip'
        WAIT: 'true'


- step:
    name: Deploy to Test
    deployment: test
    script:
      # Read results from the update pipe into environment variables
      - BITBUCKET_PIPE_SHARED_STORAGE_DIR="/opt/atlassian/pipelines/agent/build/.bitbucket/pipelines/generated/pipeline/pipes"
      - VERSION=$(jq  --raw-output '.Version' $BITBUCKET_PIPE_SHARED_STORAGE_DIR/aws-lambda-deploy-env)
      # Point the test alias to the function.
      - pipe: atlassian/aws-lambda-deploy:1.12.0
        variables:
          AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
          AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
          AWS_DEFAULT_REGION: $AWS_DEFAULT_REGION
          FUNCTION_NAME: 'my-lambda-function'
          COMMAND: 'alias'
          ALIAS: 'test'
          VERSION: '${VERSION}'
          WAIT: 'true'
      # Test the function
      - run-tests.sh

- step:
    name: Deploy to Staging
    deployment: staging
    script:
      # Read results from the update pipe into environment variables
      - BITBUCKET_PIPE_SHARED_STORAGE_DIR="/opt/atlassian/pipelines/agent/build/.bitbucket/pipelines/generated/pipeline/pipes"
      - VERSION=$(jq  --raw-output '.Version' $BITBUCKET_PIPE_SHARED_STORAGE_DIR/aws-lambda-deploy-env)
      # Point the 'staging' alias to the function.
      - pipe: atlassian/aws-lambda-deploy:1.12.0
        variables:
          AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
          AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
          AWS_DEFAULT_REGION: $AWS_DEFAULT_REGION
          FUNCTION_NAME: 'my-lambda-function'
          COMMAND: 'alias'
          ALIAS: 'staging'
          VERSION: '${VERSION}'
          WAIT: 'true'
      # Test the function
      - run-tests.sh

- step:
    name: Deploy to Production
    deployment: production
    script:
      # Read results from the update pipe into environment variables
      - BITBUCKET_PIPE_SHARED_STORAGE_DIR="/opt/atlassian/pipelines/agent/build/.bitbucket/pipelines/generated/pipeline/pipes"
      - VERSION=$(jq  --raw-output '.Version' $BITBUCKET_PIPE_SHARED_STORAGE_DIR/aws-lambda-deploy-env)
      # Point the 'production' alias to the function.
      - pipe: atlassian/aws-lambda-deploy:1.12.0
        variables:
          AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
          AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
          AWS_DEFAULT_REGION: $AWS_DEFAULT_REGION
          FUNCTION_NAME: 'my-lambda-function'
          COMMAND: 'alias'
          ALIAS: 'production'
          VERSION: '${VERSION}'
          WAIT: 'true'
      # Test the function
      - run-tests.sh
```


## Support
If you’d like help with this pipe, or you have an issue or feature request, [let us know on Community][community].

If you’re reporting an issue, please include:

* the version of the pipe
* relevant logs and error messages
* steps to reproduce


## License
Copyright (c) 2018 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.

[community]: https://community.atlassian.com/t5/forums/postpage/board-id/bitbucket-questions?add-tags=bitbucket-pipelines,pipes,aws,lambda,serverless
[aws-oidc]: https://support.atlassian.com/bitbucket-cloud/docs/deploy-on-aws-using-bitbucket-pipelines-openid-connect
[Regions and Endpoints]: https://docs.aws.amazon.com/general/latest/gr/rande.html
[boto3 lambda]: https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/lambda.html
[boto3 update_function_configuration]: https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/lambda.html#Lambda.Client.update_function_configuration
[Lambda image configuration]: https://docs.aws.amazon.com/lambda/latest/dg/configuration-images.html
[Lambda states]: https://docs.aws.amazon.com/lambda/latest/dg/functions-states.html
[Update response details of the lambda function]: https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/lambda/client/update_function_code.html
