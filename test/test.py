import os
import json
import time
import shutil
import subprocess

import boto3
import requests
import pytest
import yaml
from botocore.exceptions import WaiterError

from bitbucket_pipes_toolkit.test import PipeTestCase
from bitbucket_pipes_toolkit.helpers import get_variable


STACK_NAME = f"bbci-pipes-test-infrastructure-lambda-{get_variable('BITBUCKET_BUILD_NUMBER')}"
STACK_NAME_LAMBDA_ECR = f"bbci-pipes-test-infra-lambda-ecr-{get_variable('BITBUCKET_BUILD_NUMBER')}"
S3_BUCKET = f"bbci-pipes-test-infra-us-east-1-aws-lambda-deploy-{get_variable('BITBUCKET_BUILD_NUMBER')}"
S3_KEY_INITIAL = get_variable('S3_KEY_INITIAL')
AWS_DEFAULT_REGION = 'us-east-1'
ZIP_FILE = './test/lambda.zip'
APP_CONFIG_FILE = './test/sam-app/config.json'
LAMBDA_CONFIG_FILE = 'test/function-configuration.json'
BITBUCKET_PIPE_SHARED_STORAGE_DIR = "/opt/atlassian/pipelines/agent/build/.bitbucket/pipelines/generated/pipeline/pipes"
WAIT_MAX_ATTEMPTS_DEFAULT = 10
WAIT_INTERVAL = 5


class AwsLambdaManager:
    def __init__(self):
        self.stack_name = STACK_NAME
        self.client_cloudformation = boto3.client('cloudformation',
                                                  region_name=AWS_DEFAULT_REGION)
        self.client_lambda = boto3.client('lambda',
                                          region_name=AWS_DEFAULT_REGION)

    def get_lambda_function_name(self, output_type='Function', stack_name=None):
        return self.get_stack_output(output_type, stack_name=stack_name)['OutputValue']

    def get_lambda_function_configuration(self):
        function_name = self.get_lambda_function_name()
        return self.client_lambda.get_function_configuration(
            FunctionName=function_name)

    def get_lambda_function_alias(self, alias):
        function_name = self.get_lambda_function_name()
        return self.client_lambda.get_alias(
            FunctionName=function_name, Name=alias)

    def get_lambda_function_role_arn(self):
        return self.get_stack_output('FunctionIamRole')['OutputValue']

    def get_lambda_api(self):
        return self.get_stack_output('Api')['OutputValue']

    def get_stack_output(self, output_type, stack_name=None, output_name='HelloWorld'):
        stack_name = stack_name or self.stack_name
        stack = self.get_stack(stack_name)
        return [item for item in stack['Outputs'] if item[
            'OutputKey'] == f'{output_name}{output_type}'][0]

    def get_stack(self, stack_name):
        response = self.client_cloudformation.describe_stacks(StackName=stack_name)
        return response['Stacks'][0]

    def get_lambda_latest_version(self):
        function_name = self.get_lambda_function_name()
        return self.client_lambda.list_versions_by_function(FunctionName=function_name, MaxItems=1)

    def wait_for_function_updated(self, output_type='Function', stack_name=None):
        function_name = self.get_lambda_function_name(output_type=output_type, stack_name=stack_name)
        waiter = self.client_lambda.get_waiter('function_updated')
        try:
            waiter.wait(
                FunctionName=function_name,
                WaiterConfig={
                    'Delay': WAIT_INTERVAL,
                    'MaxAttempts': WAIT_MAX_ATTEMPTS_DEFAULT
                }
            )
        except WaiterError as error:
            raise Exception(f'Error during waiting for updated status of lambda: {error}')


class LambdaDeployFunctionTestCase(PipeTestCase):
    ALIASES = []

    @classmethod
    def setUpClass(cls):
        super().setUpClass()

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()

    def setUp(self):
        self.random = str(time.time_ns())
        self.aws_lambda_manager = AwsLambdaManager()
        self.update_app_configuration_file(self.random)
        self.create_zip_lambda_app()

    def tearDown(self):
        self.aws_lambda_manager.wait_for_function_updated()

    @staticmethod
    def create_zip_lambda_app():
        cwd = os.getcwd()
        os.chdir('test')
        shutil.make_archive('lambda', 'zip', './sam-app/')
        os.chdir(cwd)

    @staticmethod
    def update_app_configuration_file(message, config_file=APP_CONFIG_FILE):
        with open(config_file, 'w') as f:
            f.write(json.dumps({"message": str(message)}))

    @staticmethod
    def update_lambda_configuration_file(text, config_file=LAMBDA_CONFIG_FILE):
        with open(config_file, 'w') as f:
            f.write(json.dumps({"Description": str(text)}))

    @pytest.mark.order(1)
    def test_fail_if_no_params(self):
        result = self.run_container()
        self.assertRegex(result, r'AWS_DEFAULT_REGION:\n- required field')

    def test_success_update_from_s3(self):
        s3 = boto3.client('s3')

        s3.upload_file('test/lambda.zip', S3_BUCKET, 'test-s3-upload')

        result = self.run_container(
            environment={
                'AWS_ACCESS_KEY_ID': get_variable('AWS_ACCESS_KEY_ID'),
                'AWS_SECRET_ACCESS_KEY': get_variable('AWS_SECRET_ACCESS_KEY'),
                'AWS_DEFAULT_REGION': AWS_DEFAULT_REGION,
                'FUNCTION_NAME':
                    self.aws_lambda_manager.get_lambda_function_name(),
                'S3_BUCKET': S3_BUCKET,
                'S3_KEY': 'test-s3-upload',
                'COMMAND': "update",
                'BITBUCKET_PIPE_SHARED_STORAGE_DIR': BITBUCKET_PIPE_SHARED_STORAGE_DIR,
            }
        )
        self.assertIn('Update command succeeded', result)
        self.aws_lambda_manager.wait_for_function_updated()
        response = requests.get(self.aws_lambda_manager.get_lambda_api())
        self.assertIn(self.random, response.json().get('message'))

    def test_success_update(self):
        result = self.run_container(
            environment={
                'AWS_ACCESS_KEY_ID': get_variable('AWS_ACCESS_KEY_ID'),
                'AWS_SECRET_ACCESS_KEY': get_variable('AWS_SECRET_ACCESS_KEY'),
                'AWS_DEFAULT_REGION': AWS_DEFAULT_REGION,
                'FUNCTION_NAME':
                    self.aws_lambda_manager.get_lambda_function_name(),
                'ZIP_FILE': ZIP_FILE,
                'COMMAND': "update",
                'BITBUCKET_PIPE_SHARED_STORAGE_DIR': BITBUCKET_PIPE_SHARED_STORAGE_DIR,
            }
        )
        self.assertIn('Update command succeeded', result)
        self.aws_lambda_manager.wait_for_function_updated()
        response = requests.get(self.aws_lambda_manager.get_lambda_api())
        self.assertIn(self.random, response.json().get('message'))

    def test_and_wait_success_update(self):
        alias = (f"myalias-{get_variable('BITBUCKET_BUILD_NUMBER')}"
                 f"-{self.random}")
        version = self.aws_lambda_manager.get_lambda_function_configuration()['Version']
        description = 'New functionality'

        result = self.run_container(
            environment={
                'AWS_ACCESS_KEY_ID': get_variable('AWS_ACCESS_KEY_ID'),
                'AWS_SECRET_ACCESS_KEY': get_variable('AWS_SECRET_ACCESS_KEY'),
                'AWS_DEFAULT_REGION': AWS_DEFAULT_REGION,
                'FUNCTION_NAME':
                    self.aws_lambda_manager.get_lambda_function_name(),
                'COMMAND': "alias",
                'ALIAS': alias,
                'VERSION': version,
                'DESCRIPTION': description,
                'WAIT': 'true',
                'BITBUCKET_PIPE_SHARED_STORAGE_DIR': BITBUCKET_PIPE_SHARED_STORAGE_DIR
            }
        )
        self.assertIn('Lambda deployment is successful', result)
        response = self.aws_lambda_manager.get_lambda_function_alias(alias)
        self.assertIn(alias, response.get('Name'))
        self.assertIn(description, response.get('Description'))

    def test_success_update_no_publish(self):
        result = self.run_container(
            environment={
                'AWS_ACCESS_KEY_ID': get_variable('AWS_ACCESS_KEY_ID'),
                'AWS_SECRET_ACCESS_KEY': get_variable('AWS_SECRET_ACCESS_KEY'),
                'AWS_DEFAULT_REGION': AWS_DEFAULT_REGION,
                'FUNCTION_NAME':
                    self.aws_lambda_manager.get_lambda_function_name(),
                'ZIP_FILE': ZIP_FILE,
                'COMMAND': 'update',
                'PUBLISH_FLAG': False,
                'BITBUCKET_PIPE_SHARED_STORAGE_DIR': BITBUCKET_PIPE_SHARED_STORAGE_DIR,
            }
        )
        self.assertIn('Update command succeeded', result)
        self.aws_lambda_manager.wait_for_function_updated()
        response = requests.get(self.aws_lambda_manager.get_lambda_api())
        self.assertIn(self.random, response.json().get('message'))

        self.update_lambda_configuration_file(self.random)
        response = self.aws_lambda_manager.get_lambda_latest_version()
        self.assertEqual(response['Versions'][0]['Version'], '$LATEST')

    def test_success_update_oidc_authentication(self):
        """Test that passing both types of auth, oidc is used"""
        result = self.run_container(
            environment={
                'BITBUCKET_STEP_OIDC_TOKEN': get_variable('BITBUCKET_STEP_OIDC_TOKEN'),
                'AWS_OIDC_ROLE_ARN': get_variable('AWS_OIDC_ROLE_ARN'),
                'AWS_ACCESS_KEY_ID': get_variable('AWS_ACCESS_KEY_ID'),
                'AWS_SECRET_ACCESS_KEY': get_variable('AWS_SECRET_ACCESS_KEY'),
                'AWS_DEFAULT_REGION': AWS_DEFAULT_REGION,
                'FUNCTION_NAME':
                    self.aws_lambda_manager.get_lambda_function_name(),
                'ZIP_FILE': ZIP_FILE,
                'COMMAND': "update",
                'BITBUCKET_PIPE_SHARED_STORAGE_DIR': BITBUCKET_PIPE_SHARED_STORAGE_DIR,
            }
        )
        self.assertIn('Authenticating with a OpenID Connect (OIDC) Web Identity Provider', result)
        self.assertIn('Update command succeeded', result)
        self.aws_lambda_manager.wait_for_function_updated()
        response = requests.get(self.aws_lambda_manager.get_lambda_api())
        self.assertIn(self.random, response.json().get('message'))

    def test_role_arn_success(self):
        result = self.run_container(
            environment={
                'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
                'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
                'AWS_DEFAULT_REGION': os.getenv('AWS_DEFAULT_REGION'),
                'AWS_ROLE_ARN': os.getenv('AWS_ROLE_ARN'),
                'AWS_ROLE_SESSION_NAME': os.getenv('AWS_ROLE_SESSION_NAME'),
                'FUNCTION_NAME':
                    self.aws_lambda_manager.get_lambda_function_name(),
                'ZIP_FILE': ZIP_FILE,
                'COMMAND': "update",
                'BITBUCKET_PIPE_SHARED_STORAGE_DIR': BITBUCKET_PIPE_SHARED_STORAGE_DIR,
            }
        )
        self.assertIn('Using STS assume role with AWS_ROLE_ARN', result)
        self.assertIn('Update command succeeded', result)
        self.aws_lambda_manager.wait_for_function_updated()
        response = requests.get(self.aws_lambda_manager.get_lambda_api())
        self.assertIn(self.random, response.json().get('message'))

    def test_success_update_fallback_to_default_authentication(self):
        result = self.run_container(
            environment={
                'AWS_OIDC_ROLE_ARN': get_variable('AWS_OIDC_ROLE_ARN'),
                'AWS_ACCESS_KEY_ID': get_variable('AWS_ACCESS_KEY_ID'),
                'AWS_SECRET_ACCESS_KEY': get_variable('AWS_SECRET_ACCESS_KEY'),
                'AWS_DEFAULT_REGION': AWS_DEFAULT_REGION,
                'FUNCTION_NAME':
                    self.aws_lambda_manager.get_lambda_function_name(),
                'ZIP_FILE': ZIP_FILE,
                'COMMAND': "update",
                'BITBUCKET_PIPE_SHARED_STORAGE_DIR': BITBUCKET_PIPE_SHARED_STORAGE_DIR,
            }
        )
        self.assertIn('Parameter "oidc: true" in the step configuration is required for OIDC authentication', result)
        self.assertIn('Using default authentication with AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY', result)
        self.assertIn('Update command succeeded', result)
        self.aws_lambda_manager.wait_for_function_updated()
        response = requests.get(self.aws_lambda_manager.get_lambda_api())
        self.assertIn(self.random, response.json().get('message'))

    def test_success_update_from_image_uri(self):
        cwd = os.getcwd()
        os.chdir('test/sam-ecr-app')

        ecr_lambda_script = 'hello-world/app.js'
        with open(ecr_lambda_script) as f:
            message = f.read()
        message = message.replace('hello world', 'Hello world from update Lambda')
        with open(ecr_lambda_script, 'w') as f:
            f.write(message)

        sts_client = boto3.client('sts')
        account = sts_client.get_caller_identity()['Account']
        repository = f'bbci-pipes-test-ecr-lambda-{os.getenv("BITBUCKET_BUILD_NUMBER")}'
        self.image_uri = f'{account}.dkr.ecr.{os.getenv("AWS_DEFAULT_REGION")}.amazonaws.com/{repository}'

        try:
            # NOTE (galyna): we don't want to output the template in tests stdout: there is information that can be sensitive
            # (like AWS account number)
            subprocess.run('sam build'.split(), capture_output=True)
            subprocess.run(f'sam package --image-repository {self.image_uri}:helloworldfunctionecr-nodejs14.x-v1 '
                           f'--template-file .aws-sam/build/template.yaml '
                           f'--output-template-file .aws-sam/packaged.yaml'.split(),
                           check=True, capture_output=True, text=True)
        except subprocess.CalledProcessError as exc:
            raise Exception(f"Building or packaging sam-ecr-app failed due to the reason: {exc.output} {exc.stderr}")

        with open('.aws-sam/packaged.yaml') as f:
            lambda_template = yaml.safe_load(f.read())

        image_tag = lambda_template['Resources']['HelloWorldFunctionEcr']['Properties']['ImageUri'].split(':')[-1]
        result = self.run_container(
            environment={
                'AWS_ACCESS_KEY_ID': get_variable('AWS_ACCESS_KEY_ID'),
                'AWS_SECRET_ACCESS_KEY': get_variable('AWS_SECRET_ACCESS_KEY'),
                'AWS_DEFAULT_REGION': AWS_DEFAULT_REGION,
                'FUNCTION_NAME':
                    self.aws_lambda_manager.get_lambda_function_name(output_type='FunctionEcr', stack_name=STACK_NAME_LAMBDA_ECR),
                'IMAGE_URI': f'{self.image_uri}:{image_tag}',
                'COMMAND': "update",
                'BITBUCKET_PIPE_SHARED_STORAGE_DIR': BITBUCKET_PIPE_SHARED_STORAGE_DIR,
            }
        )
        os.chdir(cwd)
        self.assertIn('Update command succeeded', result)
        self.aws_lambda_manager.wait_for_function_updated(output_type='FunctionEcr', stack_name=STACK_NAME_LAMBDA_ECR)
        lambda_ecr_api = self.aws_lambda_manager.get_stack_output('Api', stack_name=STACK_NAME_LAMBDA_ECR)
        lambda_url = lambda_ecr_api['OutputValue']
        response = requests.get(lambda_url)
        self.assertIn('Hello world from update Lambda', response.json().get('message'))

    def test_success_update_configuration(self):
        self.update_lambda_configuration_file(self.random)
        result = self.run_container(
            environment={
                'AWS_ACCESS_KEY_ID': get_variable('AWS_ACCESS_KEY_ID'),
                'AWS_SECRET_ACCESS_KEY': get_variable('AWS_SECRET_ACCESS_KEY'),
                'AWS_DEFAULT_REGION': AWS_DEFAULT_REGION,
                'FUNCTION_NAME':
                    self.aws_lambda_manager.get_lambda_function_name(),
                'ZIP_FILE': ZIP_FILE,
                'COMMAND': "update",
                'FUNCTION_CONFIGURATION': LAMBDA_CONFIG_FILE,
                'BITBUCKET_PIPE_SHARED_STORAGE_DIR': BITBUCKET_PIPE_SHARED_STORAGE_DIR,
            }
        )
        self.assertIn('Lambda configuration starts update', result)

        self.assertIn(
            self.random,
            self.aws_lambda_manager.get_lambda_function_configuration()[
                'Description']
        )

    @pytest.mark.order(-2)
    def test_success_new_alias(self):
        alias = (f"myalias-{get_variable('BITBUCKET_BUILD_NUMBER')}"
                 f"-{self.random}")
        version = self.aws_lambda_manager.get_lambda_function_configuration()['Version']
        function_name = self.aws_lambda_manager.get_lambda_function_name()
        description = 'New functionality'

        result = self.run_container(
            environment={
                'AWS_ACCESS_KEY_ID': get_variable('AWS_ACCESS_KEY_ID'),
                'AWS_SECRET_ACCESS_KEY': get_variable('AWS_SECRET_ACCESS_KEY'),
                'AWS_DEFAULT_REGION': AWS_DEFAULT_REGION,
                'FUNCTION_NAME': function_name,
                'COMMAND': "alias",
                'ALIAS': alias,
                'VERSION': version,
                'DESCRIPTION': description,
                'BITBUCKET_PIPE_SHARED_STORAGE_DIR': BITBUCKET_PIPE_SHARED_STORAGE_DIR,
            }
        )
        self.ALIASES.append(alias)
        self.assertIn('Successfully created alias', result)
        response = self.aws_lambda_manager.get_lambda_function_alias(alias)
        self.assertIn(alias, response.get('Name'))
        self.assertIn(description, response.get('Description'))

    @pytest.mark.order(-1)
    def test_success_update_exist_alias(self):
        version = self.aws_lambda_manager.get_lambda_function_configuration()['Version']
        function_name = self.aws_lambda_manager.get_lambda_function_name()
        alias = self.ALIASES[0]

        result = self.run_container(
            environment={
                'AWS_ACCESS_KEY_ID': get_variable('AWS_ACCESS_KEY_ID'),
                'AWS_SECRET_ACCESS_KEY': get_variable('AWS_SECRET_ACCESS_KEY'),
                'AWS_DEFAULT_REGION': AWS_DEFAULT_REGION,
                'FUNCTION_NAME': function_name,
                'COMMAND': "alias",
                'ALIAS': alias,
                'VERSION': version,
                'BITBUCKET_PIPE_SHARED_STORAGE_DIR': BITBUCKET_PIPE_SHARED_STORAGE_DIR,
            }
        )
        self.assertIn('Successfully updated alias', result)

        response = self.aws_lambda_manager.get_lambda_function_alias(alias)
        self.assertIn(alias, response.get('Name'))
