import os
import io
import sys
import yaml
import pytest
import shutil
import importlib
import configparser
from copy import copy
import botocore.session
from botocore.stub import Stubber
from unittest import mock, TestCase
from contextlib import contextmanager


from pipe import pipe

BITBUCKET_PIPE_SHARED_STORAGE_DIR = "/opt/atlassian/pipelines/agent/build/.bitbucket/pipelines/generated/pipeline/pipes"


@contextmanager
def capture_output():
    standard_out = sys.stdout
    try:
        stdout = io.StringIO()
        sys.stdout = stdout
        yield stdout
    finally:
        sys.stdout = standard_out
        sys.stdout.flush()


class LambdaDeployTestCase(TestCase):

    def setUp(self):
        self.sys_path = copy(sys.path)
        sys.path.insert(0, os.getcwd())

    def tearDown(self):
        sys.path = self.sys_path
        test_dir = os.path.join(os.environ["HOME"], '.aws')
        if os.path.exists(test_dir):
            shutil.rmtree(test_dir)

    @mock.patch.dict(os.environ, {'AWS_OIDC_ROLE_ARN': '', 'AWS_DEFAULT_REGION': 'test-region',
                                  'AWS_ACCESS_KEY_ID': 'akiafkae', 'AWS_SECRET_ACCESS_KEY': 'secretkey',
                                  'BITBUCKET_PIPE_SHARED_STORAGE_DIR': BITBUCKET_PIPE_SHARED_STORAGE_DIR,
                                  'FUNCTION_NAME': 'test-func', 'COMMAND': 'update', 'ZIP_FILE': 'test/test_app.zip'})
    def test_discover_auth_method_default_credentials_only(self):
        from pipe import pipe

        lambda_deploy_pipe = pipe.LambdaDeployPipe(schema=pipe.schema, check_for_newer_version=True)
        lambda_deploy_pipe.auth()
        self.assertEqual(lambda_deploy_pipe.auth_method, 'DEFAULT_AUTH')
        base_config_dir = f'{os.environ["HOME"]}/.aws'

        self.assertFalse(os.path.exists(f'{base_config_dir}/.aws-oidc'))

    @mock.patch.dict(os.environ, {'BITBUCKET_STEP_OIDC_TOKEN': 'token', 'AWS_OIDC_ROLE_ARN': 'account/role',
                                  'AWS_DEFAULT_REGION': 'test-region',
                                  'BITBUCKET_PIPE_SHARED_STORAGE_DIR': BITBUCKET_PIPE_SHARED_STORAGE_DIR,
                                  'FUNCTION_NAME': 'test-func', 'COMMAND': 'update', 'ZIP_FILE': 'test/test_app.zip'})
    def test_discover_auth_method_oidc_only(self):
        from pipe import pipe

        lambda_deploy_pipe = pipe.LambdaDeployPipe(schema=pipe.schema, check_for_newer_version=True)
        lambda_deploy_pipe.auth()
        self.assertEqual(lambda_deploy_pipe.auth_method, 'OIDC_AUTH')
        self.assertTrue(os.path.exists(f'{os.environ["HOME"]}/.aws/.aws-oidc'))
        config = configparser.ConfigParser()
        config.read(f'{os.environ["HOME"]}/.aws/config')
        self.assertEqual(config['default'].get('role_arn'), 'account/role')
        token_file = config['default'].get('web_identity_token_file')

        with open(token_file) as f:
            self.assertEqual(f.read(), 'token')

    @mock.patch.dict(os.environ, {'BITBUCKET_STEP_OIDC_TOKEN': 'token', 'AWS_OIDC_ROLE_ARN': 'account/role',
                                  'AWS_ACCESS_KEY_ID': 'akiafkae', 'AWS_SECRET_ACCESS_KEY': 'secretkey',
                                  'AWS_DEFAULT_REGION': 'test-region',
                                  'BITBUCKET_PIPE_SHARED_STORAGE_DIR': BITBUCKET_PIPE_SHARED_STORAGE_DIR,
                                  'FUNCTION_NAME': 'test-func', 'COMMAND': 'update', 'ZIP_FILE': 'test/test_app.zip'})
    def test_discover_auth_method_oidc_and_default_credentials(self):
        from pipe import pipe
        self.assertTrue('AWS_ACCESS_KEY_ID' in os.environ)
        self.assertTrue('AWS_SECRET_ACCESS_KEY' in os.environ)

        lambda_deploy_pipe = pipe.LambdaDeployPipe(schema=pipe.schema, check_for_newer_version=True)
        lambda_deploy_pipe.auth()

        self.assertEqual(lambda_deploy_pipe.auth_method, 'OIDC_AUTH')
        self.assertTrue(os.path.exists(f'{os.environ["HOME"]}/.aws/.aws-oidc'))
        config = configparser.ConfigParser()
        config.read(f'{os.environ["HOME"]}/.aws/config')
        self.assertEqual(config['default'].get('role_arn'), 'account/role')
        token_file = config['default'].get('web_identity_token_file')

        with open(token_file) as f:
            self.assertEqual(f.read(), 'token')

        self.assertFalse('AWS_ACCESS_KEY_ID' in os.environ)
        self.assertFalse('AWS_SECRET_ACCESS_KEY' in os.environ)


class SuccessTestCase(TestCase):
    def setUp(self):
        self.sys_path = copy(sys.path)
        sys.path.insert(0, os.getcwd())

    def tearDown(self):
        sys.path = self.sys_path
        test_dir = os.path.join(os.environ["HOME"], '.aws')
        if os.path.exists(test_dir):
            shutil.rmtree(test_dir)

    @mock.patch.object(pipe.LambdaDeployPipe, 'get_client')
    @mock.patch.dict(
        os.environ,
        {
            'AWS_ACCESS_KEY_ID': "foo",
            'AWS_SECRET_ACCESS_KEY': "bar",
            'AWS_DEFAULT_REGION': 'baz',
            'FUNCTION_NAME': "test-function",
            'ALIAS': "alias",
            'VERSION': "version",
            'DESCRIPTION': "description",
            'COMMAND': "alias",
            'BITBUCKET_PIPE_SHARED_STORAGE_DIR': "test-dir"
        }
    )
    def test_success(self, mock_lambda):
        lambda_ = botocore.session.get_session().create_client('lambda')
        mock_lambda.return_value = lambda_

        stubber = Stubber(lambda_)

        stubber.add_response(
            'get_function',
            {
                'Configuration': {
                    'FunctionName': 'string',
                    'State': 'Active',
                    'LastUpdateStatus': 'Successful'
                }
            },
            expected_params={
                'FunctionName': 'test-function',
            }
        )

        stubber.add_response(
            'get_alias',
            {
                'Name': 'string'
            },
            expected_params={
                'FunctionName': 'test-function',
                'Name': 'alias'
            }
        )

        stubber.add_response(
            'update_alias',
            {
                'Name': 'alias'
            },
            expected_params={
                'FunctionName': 'test-function',
                'FunctionVersion': 'version',
                'Name': 'alias',
                'Description': 'description'
            }
        )

        current_pipe_module = importlib.import_module('pipe.pipe')

        with open(os.path.join(os.getcwd(), 'pipe.yml')) as f:
            metadata = yaml.safe_load(f.read())
        lambda_pipe = current_pipe_module.LambdaDeployPipe(
            schema=current_pipe_module.schema,
            pipe_metadata=metadata,
            check_for_newer_version=True,
        )

        with stubber:
            with capture_output() as out:
                lambda_pipe.run()

        self.assertRegex(
            out.getvalue(),
            r"✔ Lambda is ready for deployment."
        )


class NegativeTestCase(TestCase):
    def setUp(self):
        self.sys_path = copy(sys.path)
        sys.path.insert(0, os.getcwd())

    def tearDown(self):
        sys.path = self.sys_path
        test_dir = os.path.join(os.environ["HOME"], '.aws')
        if os.path.exists(test_dir):
            shutil.rmtree(test_dir)

    @mock.patch.object(pipe.LambdaDeployPipe, 'get_client')
    @mock.patch.dict(
        os.environ,
        {
            'AWS_ACCESS_KEY_ID': "foo",
            'AWS_SECRET_ACCESS_KEY': "bar",
            'AWS_DEFAULT_REGION': 'baz',
            'FUNCTION_NAME': "test-function",
            'ALIAS': "alias",
            'VERSION': "version",
            'DESCRIPTION': "description",
            'COMMAND': "alias",
            'BITBUCKET_PIPE_SHARED_STORAGE_DIR': "test-dir"
        }
    )
    def test_fail_lambda_check(self, mock_lambda):
        lambda_ = botocore.session.get_session().create_client('lambda')
        mock_lambda.return_value = lambda_

        stubber = Stubber(lambda_)

        stubber.add_response(
            'get_function',
            {
                'Configuration': {
                    'FunctionName': 'string',
                    'State': 'Active',
                    'LastUpdateStatus': 'InProgress'
                }
            },
            expected_params={
                'FunctionName': 'test-function',
            }
        )

        stubber.add_response(
            'update_alias',
            {
                'Name': 'alias'
            },
            expected_params={
                'FunctionName': 'test-function',
                'FunctionVersion': 'version',
                'Name': 'alias',
                'Description': 'description'
            }
        )

        current_pipe_module = importlib.import_module('pipe.pipe')

        with open(os.path.join(os.getcwd(), 'pipe.yml')) as f:
            metadata = yaml.safe_load(f.read())

        with stubber:
            with capture_output() as out:
                with pytest.raises(SystemExit) as pytest_wrapped_e:
                    lambda_pipe = current_pipe_module.LambdaDeployPipe(
                        schema=current_pipe_module.schema,
                        pipe_metadata=metadata,
                        check_for_newer_version=True,
                    )
                    lambda_pipe.run()

        self.assertEqual(pytest_wrapped_e.type, SystemExit)
        self.assertRegex(
            out.getvalue(),
            r"✖ Lambda is not ready for deployment. Lambda current State is Active, current LastUpdateStatus is InProgress"
        )
