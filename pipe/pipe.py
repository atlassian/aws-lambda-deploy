import os
import json
import stat
import time
import logging
import pathlib

import yaml
import boto3
import configparser
from botocore.exceptions import ClientError, WaiterError

from bitbucket_pipes_toolkit import Pipe, get_logger


WAIT_INTERVAL_DEFAULT = 5  # sec
WAIT_MAX_ATTEMPTS_DEFAULT = 60

schema = {
    # base
    'AWS_ACCESS_KEY_ID': {'type': 'string', 'required': True},
    'AWS_SECRET_ACCESS_KEY': {'type': 'string', 'required': True},
    'AWS_DEFAULT_REGION': {'type': 'string', 'required': True},
    'AWS_ROLE_ARN': {'type': 'string', 'required': False},
    'AWS_ROLE_SESSION_NAME': {'type': 'string', 'required': False},
    'FUNCTION_NAME': {'type': 'string', 'required': True},
    'COMMAND': {'type': 'string', 'required': True, 'allowed': ['update', 'alias']},
    # common
    'FUNCTION_CONFIGURATION': {'type': 'string', 'required': False, 'default': ''},
    "WAIT": {'type': 'boolean', 'default': False},
    'WAIT_INTERVAL': {'type': 'number', 'required': False, 'default': WAIT_INTERVAL_DEFAULT},
    'DEBUG': {'type': 'boolean', 'required': False, 'default': False},
}

update_schema = {
    # update
    'ZIP_FILE': {'type': 'string', 'required': False},
    'IMAGE_URI': {'type': 'string', 'required': False},
    'S3_BUCKET': {'type': 'string', 'required': False},
    'S3_KEY': {'type': 'string', 'required': False},
    'S3_OBJECT_VERSION': {'type': 'string', 'required': False},
    'PUBLISH_FLAG': {'type': 'boolean', 'required': False, 'default': True},
}

alias_schema = {
    # alias
    'ALIAS': {'type': 'string', 'required': True},
    'VERSION': {'type': 'string', 'required': True},
    'DESCRIPTION': {'type': 'string', 'required': False, 'default': ''},
}


logger = get_logger()


class LambdaDeployPipe(Pipe):
    OIDC_AUTH = 'OIDC_AUTH'
    DEFAULT_AUTH = 'DEFAULT_AUTH'
    ACTIVE_STATE = 'Active'
    INACTIVE_STATE = 'Inactive'
    SUCCESS_UPDATE_STATUS = 'Successful'
    IN_PROGRESS_UPDATE_STATUS = 'InProgress'

    AWS_LAMBDA_DEPLOY_ENV_FILENAME = 'aws-lambda-deploy-env'

    def __init__(self, *args, **kwargs):
        self.auth_method = self.discover_auth_method()
        self.role_arn_check = self.resolve_role_arn()
        super().__init__(*args, **kwargs)
        self.client = None
        self.update_response_file_path = os.path.join(
            os.getenv('BITBUCKET_PIPE_SHARED_STORAGE_DIR'), self.AWS_LAMBDA_DEPLOY_ENV_FILENAME)

        self.region = self.get_variable('AWS_DEFAULT_REGION')

    def discover_auth_method(self):
        """Discover user intentions: authenticate to AWS through OIDC or default aws access keys"""
        oidc_role = os.getenv('AWS_OIDC_ROLE_ARN')
        web_identity_token = os.getenv('BITBUCKET_STEP_OIDC_TOKEN')
        if oidc_role:
            if web_identity_token:
                logger.info('Authenticating with a OpenID Connect (OIDC) Web Identity Provider')
                schema['BITBUCKET_STEP_OIDC_TOKEN'] = {'required': True}
                schema['AWS_OIDC_ROLE_ARN'] = {'required': True}

                schema['AWS_ACCESS_KEY_ID']['required'] = False
                schema['AWS_SECRET_ACCESS_KEY']['required'] = False

                # unset env variables to prevent aws client general auth
                # https://boto3.amazonaws.com/v1/documentation/api/latest/guide/credentials.html
                if 'AWS_ACCESS_KEY_ID' in os.environ:
                    del os.environ['AWS_ACCESS_KEY_ID']
                if 'AWS_SECRET_ACCESS_KEY' in os.environ:
                    del os.environ['AWS_SECRET_ACCESS_KEY']
                if 'AWS_SESSION_TOKEN' in os.environ:
                    del os.environ['AWS_SESSION_TOKEN']

                return self.OIDC_AUTH

            logger.warning('Parameter "oidc: true" in the step configuration is required for OIDC authentication')
            logger.info('Using default authentication with AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY')
            return self.DEFAULT_AUTH

        logger.info('Using default authentication with AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY')
        return self.DEFAULT_AUTH

    @staticmethod
    def resolve_role_arn():
        if os.getenv('AWS_ROLE_ARN'):
            schema['AWS_ROLE_ARN']['required'] = True
            schema['AWS_ROLE_SESSION_NAME']['required'] = True

            return True

        return False

    def auth(self):
        """Authenticate via chosen method"""
        if self.auth_method == self.OIDC_AUTH:
            random_number = str(time.time_ns())
            aws_config_directory = os.path.join(os.environ["HOME"], '.aws')
            oidc_token_directory = os.path.join(aws_config_directory, '.aws-oidc')

            os.makedirs(aws_config_directory, exist_ok=True)
            os.makedirs(oidc_token_directory, exist_ok=True)

            web_identity_token_path = os.path.join(oidc_token_directory, f'oidc_token_{random_number}')
            with open(web_identity_token_path, 'w') as f:
                f.write(self.get_variable('BITBUCKET_STEP_OIDC_TOKEN'))

            os.chmod(web_identity_token_path, mode=stat.S_IRUSR)
            logger.debug('Web identity token file is created')

            aws_configfile_path = os.path.join(aws_config_directory, 'config')
            with open(aws_configfile_path, 'w') as configfile:
                config = configparser.ConfigParser()
                config['default'] = {
                    'role_arn': self.get_variable('AWS_OIDC_ROLE_ARN'),
                    'web_identity_token_file': web_identity_token_path
                }
                config.write(configfile)
            logger.debug('Configured settings for authentication with assume web identity role')

    def get_client(self):
        try:
            if self.role_arn_check:
                role_arn = self.get_variable('AWS_ROLE_ARN')
                role_session_name = self.get_variable('AWS_ROLE_SESSION_NAME')
                logger.info('Using STS assume role with AWS_ROLE_ARN')

                session = self.role_arn_to_session(role_arn, role_session_name)

                return session.client('lambda', region_name=self.region)

            return boto3.client('lambda', region_name=self.region)
        except ClientError as error:
            self.fail(f"Failed to create boto3 client.\n {str(error)}")

    @staticmethod
    def role_arn_to_session(role_arn, role_session_name):
        client = boto3.client('sts')
        response = client.assume_role(
            RoleArn=role_arn,
            RoleSessionName=role_session_name
        )

        return boto3.Session(
            aws_access_key_id=response['Credentials']['AccessKeyId'],
            aws_secret_access_key=response['Credentials']['SecretAccessKey'],
            aws_session_token=response['Credentials']['SessionToken'])

    def update_function_configuration(self, config_path):
        self.log_info('Updating Lambda function configuration.')

        lambda_configuration = {}

        try:
            with open(config_path, 'rb') as config_file:
                lambda_configuration.update(json.load(config_file))
        except json.JSONDecodeError as error:
            self.fail(f"Failed to decode Lambda function configuration file. Error: {error}")

        lambda_configuration['FunctionName'] = self.get_variable('FUNCTION_NAME')

        try:
            self.client.update_function_configuration(**lambda_configuration)
        except ClientError as error:
            self.fail(f"Failed to update Lambda function configuration. Error: {error}")

        self.success("Lambda configuration starts update.")

    def update_or_create_alias(self):
        try:
            self.get_alias()
        except ClientError:
            self.create_alias()

        self.update_alias()

    def get_alias(self):
        self.log_info("Checking for existing lambda alias.")
        return self.client.get_alias(
            FunctionName=self.get_variable('FUNCTION_NAME'),
            Name=self.get_variable('ALIAS')
        )

    def update_alias(self):
        self.log_info("Existing alias found - updating alias.")

        try:
            self.client.update_alias(
                FunctionName=self.get_variable('FUNCTION_NAME'),
                FunctionVersion=self.get_variable('VERSION'),
                Name=self.get_variable('ALIAS'),
                Description=self.get_variable('DESCRIPTION'),
            )
        except ClientError:
            self.fail("Failed to update alias")

        self.success("Successfully updated alias.")

    def create_alias(self):
        self.log_info("Existing alias not found - creating new alias.")

        try:
            self.client.create_alias(
                FunctionName=self.get_variable('FUNCTION_NAME'),
                FunctionVersion=self.get_variable('VERSION'),
                Name=self.get_variable('ALIAS'),
                Description=self.get_variable('DESCRIPTION'),
            )
        except ClientError:
            self.fail("Failed to create alias")

        self.success("Successfully created alias.")

    def update_function_code(self):
        self.log_info("Updating Lambda function.")

        zip_file_path = self.get_variable('ZIP_FILE')
        image_uri = self.get_variable('IMAGE_URI')
        # AWS S3 bucket
        s3_bucket = self.get_variable('S3_BUCKET')
        s3_key = self.get_variable('S3_KEY')
        s3_object_version = self.get_variable('S3_OBJECT_VERSION')

        if not any([zip_file_path, image_uri, s3_bucket]):
            self.fail('Validation error: either ZIP_FILE, IMAGE_URI or S3_BUCKET variable should be passed.')
        if zip_file_path and not pathlib.Path(zip_file_path).exists():
            self.fail("ZIP_FILE not exists.")

        source = {}
        if zip_file_path:
            with open(zip_file_path, 'rb') as zip_file:
                source = {'ZipFile': zip_file.read()}
        elif image_uri:
            source = {'ImageUri': image_uri}
        elif s3_bucket:
            if not s3_key:
                self.fail('Validation error: S3_KEY variable should be passed.')
            source = {'S3Bucket': s3_bucket, 'S3Key': s3_key}
            if s3_object_version:
                source['S3ObjectVersion'] = s3_object_version

        try:
            response = self.client.update_function_code(
                FunctionName=self.get_variable('FUNCTION_NAME'),
                Publish=self.get_variable('PUBLISH_FLAG'),
                **source
            )
        except ClientError as error:
            self.fail(f"Failed to update Lambda function code. Error: {error}")

        self.log_info("Update command succeeded.")
        self.write_update_response_to_file(response)
        self.success("Successfully updated function.")

    def write_update_response_to_file(self, data):
        # try to store data to a shared data storage
        self.log_info(f"Writing results to file {self.update_response_file_path}")

        with open(self.update_response_file_path, 'w') as result_file:
            result_file.write(json.dumps(data))

        self.log_info(f"Results stored in json format in the file {self.AWS_LAMBDA_DEPLOY_ENV_FILENAME} "
                      f"under $BITBUCKET_PIPE_SHARED_STORAGE_DIR directory.")

    def fetch_data_from_file(self):
        # try to fetch from shared data storage
        with open(self.update_response_file_path, 'r') as result_file:
            return json.loads(result_file.read())

    def check_lamda_state_and_status(self):
        """Ensure lambda is in 'Active' or 'Inactive' State
        and not currently updating (LastUpdateStatus != 'InProgress'):
        https://aws.amazon.com/blogs/compute/tracking-the-state-of-lambda-functions/
        """
        self.log_info("Performing check lambda is in 'Active' or 'Inactive' State and not in 'InProgres' status.")
        response = self.client.get_function(
            FunctionName=self.get_variable('FUNCTION_NAME')
        )

        if (
                response['Configuration']['State'] not in (self.ACTIVE_STATE, self.INACTIVE_STATE)
                or response['Configuration']['LastUpdateStatus'] == self.IN_PROGRESS_UPDATE_STATUS
        ):
            self.fail(
                f"Lambda is not ready for deployment."
                f" Lambda current State is {response['Configuration']['State']},"
                f" current LastUpdateStatus is {response['Configuration']['LastUpdateStatus']}"
            )

        self.success("Lambda is ready for deployment.")

    def wait_for_function_updated(self):
        waiter = self.client.get_waiter('function_updated')
        wait_interval = self.get_variable('WAIT_INTERVAL')
        try:
            waiter.wait(
                FunctionName=self.get_variable('FUNCTION_NAME'),
                WaiterConfig={
                    'Delay': wait_interval,
                    'MaxAttempts': WAIT_MAX_ATTEMPTS_DEFAULT
                }
            )
        except WaiterError as error:
            self.log_info(
                self.client.get_function_configuration(self.get_variable('FUNCTION_NAME')))
            self.fail(f'Error during waiting for updated status of lambda: {error}')

        self.success("Lambda 'LastUpdateStatus' is 'Successful'.")

    def run(self):
        super().run()

        self.log_info('Executing the aws-lambda-deploy pipe...')
        boto3.set_stream_logger('boto3.resources', logging.INFO)

        if self.get_variable('DEBUG'):
            boto3.set_stream_logger('boto3.resources', logging.DEBUG)

        self.auth()
        self.client = self.get_client()

        # Check for lambda is in required state and status.
        self.check_lamda_state_and_status()

        function_config_path = self.get_variable('FUNCTION_CONFIGURATION')

        if function_config_path:
            if not pathlib.Path(function_config_path).exists():
                self.fail("Path provided in FUNCTION_CONFIGURATION variable not exists.")
            self.update_function_configuration(function_config_path)

            self.wait_for_function_updated()

        if self.get_variable("COMMAND") == 'alias':
            # try to fetch from shared data storage
            if not os.getenv('VERSION') and pathlib.Path(self.update_response_file_path).exists():
                data = self.fetch_data_from_file()
                os.environ['VERSION'] = data.get('Version')

            self.schema.update(alias_schema)
            variables = self.validate()
            self.variables.update(variables)
            self.update_or_create_alias()
        else:
            self.schema.update(update_schema)
            variables = self.validate()
            self.variables.update(variables)
            self.update_function_code()

        if self.get_variable('WAIT'):
            logger.info("Waiting for lambda LastUpdateStatus 'Successful'")
            self.wait_for_function_updated()
            self.success("Lambda deployment is successful.")


if __name__ == '__main__':
    with open('/pipe.yml', 'r') as metadata_file:
        metadata = yaml.safe_load(metadata_file.read())
        pipe = LambdaDeployPipe(schema=schema, pipe_metadata=metadata, check_for_newer_version=True)
        pipe.run()
