# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 1.12.0

- minor: Implemented support for AWS STS (using assume-role functionality).

## 1.11.3

- patch: Internal maintenance: Bump cryptography package version to fix vulnerabilities.
- patch: Internal maintenance: bump project's dependencies to the latest.

## 1.11.2

- patch: Internal maintenance: Bump release pipe version. Add multi-platform build: linux/arm64 and linux/amd64.

## 1.11.1

- patch: Internal maintenance: bump project's dependencies to latest.
- patch: Update and clarify the Readme with details about update lambda command and results about lambda version. The pipe will create an aws-lambda-deploy-env file under the BITBUCKET_PIPE_SHARED_STORAGE_DIR directory when executing the update command, containing the FunctionArn, Version and other function data in a json format.

## 1.11.0

- minor: Bump bitbucket-pipes-toolkit to fix vulnerabilities.

## 1.10.1

- patch: Internal maintenance: update pipe versions in pipeline config file.
- patch: Internal maintenance: update requirements.
- patch: Update test files to fix vulnerability with axios.
- patch: Update variables description in README.

## 1.10.0

- minor: Bump bitbucket-pipes-toolkit to fix vulnerabilities.

## 1.9.0

- minor: Update bitbucket-pipes-toolkit and aws-sam-cli to fix vulnerability with certify.

## 1.8.2

- patch: Internal maintenance: bump aws-sam-cli version to 1.76.0.

## 1.8.1

- patch: Internal maintenance: bunp aws-sam-cli version to 1.73.0.

## 1.8.0

- minor: Check lambda should be in Active/Inactive state and not InProgress status before deploy.
- patch: Internal maintenance: Update packages, docker images, and pipes versions in Dockerfile, pipeline config file, and requirements.

## 1.7.0

- minor: Add support for S3_BUCKET, S3_KEY and S3_OBJECT_VERSION variables.

## 1.6.0

- minor: Fix security vulnerability with cookiecutter: bump version of awssam cli.
- patch: Internal maintenance: Bump version of the bitbucket-pipe-release.
- patch: Internal maintenance: Update community link.
- patch: Update README with additional IAM permissions.

## 1.5.0

- minor: Update README examples.

## 1.4.0

- minor: Add feature check the lambda to be in active state and successful last update status before executing. Add wait for successful update state after lambda configuration update. Add support for wait until lambda update is successful.
- minor: Bump boto3 version to 1.20.*.
- patch: Internal maintenance: bump pipe versions.

## 1.3.0

- minor: Add support for WAIT_INTERVAL variable. Time to wait between polling for lambda configuration update to complete.
- minor: Fix error with Lambda state between configuration and code updates.

## 1.2.0

- minor: Bump boto3 to 1.18.* version.
- minor: Internal maintenance: Bump dependency bitbucket-pipes-toolkit to 3.2.0 version.

## 1.1.0

- minor: Support lambda deployment from ECR container image.

## 1.0.0

- major: Migrate pipe's codebase to Python. Now the pipe is based on AWS Boto3 lambda. Breaking Changes: parameters passed to variables should be AWS boto3 lambda related https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/lambda.html.

## 0.8.0

- minor: Make publishing lambda configurable.

## 0.7.0

- minor: Bump awscli -> 1.18.188.
- minor: Support AWS OIDC authentication. Environment variables as aws access keys are not required anymore.

## 0.6.0

- minor: Add support for DESCRIPTION parameter in the ‘alias’ command.

## 0.5.7

- patch: Bugfix: enabling debug.

## 0.5.6

- patch: Internal maintenance: bump bitbucket-pipe-release.

## 0.5.5

- patch: Fix an example shared storage dir in the Readme.

## 0.5.4

- patch: Internal maintenance: change pipe metadata according to new structure

## 0.5.3

- patch: Internal maintenance: Add gitignore secrets.

## 0.5.2

- patch: Internal maintenance: Add auto infrastructure for tests.

## 0.5.1

- patch: Internal maintenance: Upgrade dependency version bitbucket-pipes-toolkit.
- patch: Update the Readme with a new Atlassian Community link.
- patch: Update the Readme with details about default variables.

## 0.5.0

- minor: Add default values for AWS variables.

## 0.4.3

- patch: Internal maintenance: Add hadolint linter for Dockerfile

## 0.4.2

- patch: Internal maintenance: Refactor tests to python.

## 0.4.1

- patch: Internal release

## 0.4.0

- minor: Support updating function configuration

## 0.3.2

- patch: Documentation updates

## 0.3.1

- patch: Refactor pipe code to use pipes bash toolkit.
- patch: Update aws-cli base docker image version.

## 0.3.0

- minor: Use pipes data sharing to store data

## 0.2.3

- patch: Minor documentation update

## 0.2.2

- patch: Updated contributing guidelines

## 0.2.1

- patch: Standardising README and pipes.yml.

## 0.2.0

- minor: Adopt new naming and consistency conventions.

## 0.1.2

- patch: Use quotes for all pipes examples in README.md.

## 0.1.1

- patch: Restructure README.md to match user flow.

## 0.1.0

- patch: Initial release of Bitbucket Pipelines AWS Lambda update pipe.
